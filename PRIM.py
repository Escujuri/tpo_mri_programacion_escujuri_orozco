# PRIM en Python

#Uso un numero grande para entrar en el minimo:
INF = 99999999999999999999999999999999999999

# el número de vertices del grafo
V = 3

# creamos matriz de adyacencia de 3x3
G = [[0, 20, 1],
     [20, 0, 5],
     [1, 5, 0]]

# creamos el array seleccionado para ver 
# como flag para ver cual tengo "seleccionado"
seleccionado = [0, 0, 0]

# ponemos el aux_arista en 0
aux_arista = 0

# seleccionamos el 0 para arrancar y lo ponemos en True:

seleccionado[0] = True
print("PRIM ARCM:\n")
while (aux_arista < V - 1):
    
    # Por cada lugar de la matriz, busco todos los vertices adyacentes
    # y calculo la arista
    # Si el vertice esta agregado, lo descarto.
    # sino, elijo el vertice mas cercano del vertice seleccionado

    minimo = INF
    x = 0
    y = 0
    for i in range(V):
        if seleccionado[i]:
            for j in range(V):
                if ((not seleccionado[j]) and G[i][j]):  
                    # no esta en "seleccionado" y tengo una arista por agregar
                    # print("Minimo: " + str(minimo) + " G de ij:" + str(G[x][y]) + "\n")
                    if minimo > G[i][j]:
                        minimo = G[i][j]
                        x = i
                        y = j
                    #    print("Devuelvo: " + str(G[x][y]) + "\n")
    print("De Vertice " + str(x) + " al --------> " + str(y) + " con valor: " + str(G[x][y]))
    seleccionado[y] = True
    aux_arista += 1