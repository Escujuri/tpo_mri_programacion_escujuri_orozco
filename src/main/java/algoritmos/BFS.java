package algoritmos;

import java.util.ArrayList;

import contract.GrafosTDA;
import service.NodoArista;
import service.NodoGrafo;

public class BFS {

	
	public static ArrayList<Integer> algoritmoBfs (GrafosTDA grafo, NodoGrafo origen) {
		ArrayList<Integer> arbol = new ArrayList<Integer>();
		
		if(grafo.pertenece(origen.valor)) {
			recorridoNodos(origen, arbol);
			
		}
		return arbol;
	}

	public static void recorridoNodos(NodoGrafo nodo, ArrayList<Integer> lista) {		
		
		NodoArista arbol = nodo.lista;

		//Agrego lista en la primer iteracion al nodo inicial
		if (!nodo.Visitado) {
			nodo.Visitado = true;
			lista.add(nodo.valor);
		} 
		
		//Recorres los nodos adyacentes buscando si fue visitado y marcando en tal caso agregando al arreglo
		while (arbol != null) {
			
			if (!arbol.apunta.Visitado) {
				arbol.apunta.Visitado = true;
				lista.add(arbol.apunta.valor);				
			} 
			arbol = arbol.sig;
		} 
		
		arbol = nodo.lista;
		
		// Recursividad al siguiente nodo 
		while (arbol != null) {
			if (!arbol.Visitado) {
				arbol.Visitado = true;
				recorridoNodos(arbol.apunta, lista);
			}
			arbol = arbol.sig; 
		}
	}
}
