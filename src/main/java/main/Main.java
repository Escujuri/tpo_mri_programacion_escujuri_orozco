package main;

import java.util.ArrayList;

import algoritmos.BFS;
import contract.GrafosTDA;
import service.GrafoDinamic;
import service.NodoGrafo;

public class Main {

	public static void main(String[] args) {
	
		BFS algoritmo = new BFS();
		
		// Generamos un mock de un grafo no dirigido y ponderado.
		 GrafosTDA grafo_1 = new GrafoDinamic();
		 
		 grafo_1.inicializarGrafo(5);
		 
		 grafo_1.agregarVertice(1);
		 grafo_1.agregarVertice(2);
		 grafo_1.agregarVertice(3);
		 grafo_1.agregarVertice(4);
		 grafo_1.agregarVertice(5);
		 
		 
		 // Al ser un grafo no dirigido cargamos ambas relaciones
		 
		 grafo_1.agregarArista(1, 2, 1);
		 grafo_1.agregarArista(2, 1, 1);

		 
		 grafo_1.agregarArista(2, 3, 1);
		 grafo_1.agregarArista(3, 2, 1);
		 
		 
		 grafo_1.agregarArista(3, 4, 1);
		 grafo_1.agregarArista(4, 3, 1);
		 
		 
		 
		 grafo_1.agregarArista(4, 5, 1);
		 grafo_1.agregarArista(5, 4, 1);
		 
		 
		 grafo_1.agregarArista(2, 4, 1);
		 grafo_1.agregarArista(4, 2, 1);
		 
		 
		 grafo_1.agregarArista(1, 5, 1);
		 grafo_1.agregarArista(5, 1, 1);
		 
		 
		 // Se agrego al metodo del TDA asi obtengo el nodo origen que utilizare para la busqueda
		 NodoGrafo origen = grafo_1.encontrarNodo(3);
	
		 ArrayList<Integer> resultado = algoritmo.algoritmoBfs(grafo_1, origen);



		 
		 
		 
		 
		 
	}

}
